<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Página principal
Route::get('/', 'ProductController@getIndex');

//Página carrito
Route::get('/carrito', 'ProductController@getCarrito');

//Página productos
Route::get('/productos', 'ProductController@getProducts');

//Página producto
Route::get('/productos/{nombre}', 'ProductController@getProduct');

//Página registro (sólo si no estás logueado)
Route::get('/registro', ['uses' => 'UserController@getRegistro', 'before' => 'guest']);
Route::post('/registro', ['uses' => 'UserController@postRegistro', 'before' => 'guest']);

//Página formulario login (sólo si no estás logueado)
Route::get('/login', ['uses' => 'UserController@getLogin', 'before' => 'guest']);
Route::post('/login', ['uses' => 'UserController@postLogin', 'before' => 'guest']);
Route::get('/logout', ['uses' => 'UserController@getLogout', 'before' => 'auth']);

//Página formulario de compra (sólo si estás logueado)
Route::get('/formulario_compra', ['uses' => 'ProductController@getFormularioCompra', 'before' => 'auth']);

//Página perfil (sólo si estás logueado)
Route::get('/perfil', ['uses' => 'UserController@getPerfil', 'before' => 'auth']);
Route::post('/perfil', ['uses' => 'UserController@postPerfil', 'before' => 'auth']);

//Añadir y eliminar producto
Route::post('/add/{nombre}', 'ProductController@postAdd');
Route::post('/remove/{nombre}', 'ProductController@postDelete');

//Obtener datos usuarios
Route::post('/usuario', ['uses' => 'UserController@datosUsuario', 'before' => 'auth']);

Route::post('/procesar', ['uses' => 'ProductController@postProcesar', 'before' => 'auth']);


//Finalizar compra
Route::get('/finish', ['uses' => 'ProductController@getFinish', 'before' => 'auth']);