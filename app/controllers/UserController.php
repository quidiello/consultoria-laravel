<?php

class UserController extends BaseController {

	//Vista perfil
	public function getPerfil()
	{
		$usuario = Auth::user();
		return View::make('perfil')->with('usuario', $usuario);
	}

	//Procesar perfil
	public function postPerfil()
	{
		//Obtenemos datos
		$nombre = strip_tags(Input::get('name'));
		$apellidos = strip_tags(Input::get('surnames'));
		$telefono = strip_tags(Input::get('phone'));
		$pais = strip_tags(Input::get('country'));
		$provincia = strip_tags(Input::get('province'));
		$ciudad = strip_tags(Input::get('city'));
		$cp = strip_tags(Input::get('cp'));
		$direccion = strip_tags(Input::get('street'));

		$user = User::find(Auth::user()->id);
		$user->nombre = $nombre;
		$user->apellidos = $apellidos;
		$user->telefono = $telefono;
		$user->pais = $pais;
		$user->provincia = $provincia;
		$user->ciudad = $ciudad;
		$user->cp = $cp;
		$user->direccion = $direccion;
		$user->save();
	}

	//Obtener datos del usuario
	public function datosUsuario()
	{
		return $user = Auth::user();
	}

	//Vista login
	public function getLogin()
	{
		return View::make('login');
	}

	//Procesa login
	public function postLogin()
	{
		//Obtenemos email y password
		$email = e(mb_strtolower(trim(Input::get('email'))));
		$password = e(Input::get('password'));

		//Realizamos autentificación
		if(Auth::attempt(['email' => $email, 'password' => $password]))
		{
			//Autentificación correcta
			return Redirect::to('/');
		}

		//Autentificación fallida
		return Redirect::back()->with('msg', 'Datos incorrectos, vuelva a intentarlo.');
	}

	//Destruye sesión
	public function getLogout()
	{
		//Desconectamos al usuario
		Auth::logout();
		//Creamos nueva sesión
		Session::flush();
		//Volvemos a la vista anterior
		return Redirect::back();
	}

	//Vista registro
	public function getRegistro()
	{
		return View::make('registro');
	}

	//Procesar registro
	public function postRegistro()
	{
		//Obtenemos datos registro
		$nombre = strip_tags(Input::get('name'));
		$apellidos = strip_tags(Input::get('surnames'));
		$email = strip_tags(Input::get('email'));
		$password = strip_tags(Input::get('password'));
		$password2 = strip_tags(Input::get('password2'));

		$data = array(
			'nombre' => $nombre,
			'apellidos' => $apellidos,
			'email' => $email,
			'password' => $password,
			'password2' => $password2
		);

		//Creamos un array con las reglas
		$rules = array(
			'nombre' => 'required',
			'apellidos' => 'required',
			'email' => 'required | email | unique:users',
			'password' => 'required',
			'password2' => 'required | same:password'
		);

		//Perdonalizamos mensajes de error
		$messages = array(
			'required' => 'El campo :attribute es obligatorio.',
			'email' => 'El campo :attribute ha ser un email válido.',
			'same' => 'El :attribute y :other deben coincidir.'
		);

		//Validamos
		$validation = Validator::make($data, $rules, $messages);

		//Comprobamos si los datos son erróneos
		if($validation->fails())
		{
			$errores = $validation->errors()->toArray();
			$mensaje = '';
			if(array_key_exists('nombre', $errores))
			{
				foreach($errores['nombre'] as $error)
				{
					$mensaje .= $error."<br />";
				}
			}
			if(array_key_exists('apellidos', $errores))
			{
				foreach($errores['apellidos'] as $error)
				{
					$mensaje .= $error."<br />";
				}
			}
			if(array_key_exists('email', $errores))
			{
				foreach($errores['email'] as $error)
				{
					$mensaje .= $error."<br />";
				}
			}
			if(array_key_exists('password', $errores))
			{
				foreach($errores['password'] as $error)
				{
					$mensaje .= $error."<br />";
				}
			}
			if(array_key_exists('password2', $errores))
			{
				foreach($errores['password2'] as $error)
				{
					$mensaje .= $error."<br />";
				}
			}
			if(array_key_exists('nombre', $errores))
			{
				foreach($errores['nombre'] as $error)
				{
					$mensaje .= $error."<br />";
				}
			}
			return Redirect::back()->with('msg', $mensaje);
		}

		$user = new User;
		$user->nombre = $nombre;
		$user->apellidos = $apellidos;
		$user->email = $email;
		$user->password = Hash::make($password);
		$user->save();

		return Redirect::to('/login')->with('msg', 'El usuario se ha creado correctamente.');
	}

}
