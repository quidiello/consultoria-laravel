<?php

class ProductController extends BaseController {

	//Vista principal
	public function getIndex()
	{
		$productos = Product::all();
		return View::make('inicio')->with('productos', $productos);
	}

	//Vista productos
	public function getProducts()
	{
		$productos = Product::all();
		return View::make('productos')->with('productos', $productos);
	}

	//Vista producto
	public function getProduct($nombre)
	{
		$producto = Product::where('nombre_corto', '=', $nombre)->first();
		if(is_null($producto))
		{
			return Redirect::to('/productos');
		}
		return View::make('producto')->with('producto', $producto);
	}

	//Vista carrito
	public function getCarrito()
	{
		return View::make('carrito');
	}

	//Vista formulario compra
	public function getFormularioCompra()
	{
        $importe_total = Product::total() * 100; // formato requerido por el tpv de Cajastur 2.56 serian 256
        $amount = number_format(Product::total(), 2);
        $num_operacion = Order::lastOrder() + 1 ;

		if(Session::has('carrito') && count(Session::get('carrito')))
		{
			return View::make('formulario')
                ->with('cajastur', new Cajastur($num_operacion, "", $importe_total))
                ->with('paypal', new Paypal($amount));
		}
		return Redirect::to('/carrito');
	}

	//Añadir producto carrito
	public function postAdd($nombre)
	{
		$retorno = 0;
		if(Session::has('carrito')) {
			$carrito = Session::get('carrito');
			if(! in_array($nombre, $carrito))
			{
				array_push($carrito, $nombre);
				Session::put('carrito', $carrito);
			}
			else {
				$retorno = 1;
			}
		}
		else {
			$carrito = array($nombre);
			Session::put('carrito', $carrito);
		}

		return $retorno;
	}

	//Eliminar producto carrito
	public function postDelete($nombre)
	{
		$retorno = 0;
		if(Session::has('carrito')) {
			$carrito = Session::get('carrito');
			if(($key = array_search($nombre, $carrito)) !== false) {
				unset($carrito[$key]);
				Session::put('carrito', $carrito);
			}
			else {
				$retorno = 1;
			}
		}
		return $retorno;
	}

	//Procesar pago
	public function postProcesar(){
		if(Session::has('carrito') && count(Session::get('carrito')))
		{
			//Obtenemos datos
			$nombre = strip_tags(Input::get('name'));
			$apellidos = strip_tags(Input::get('surnames'));
			$email = strip_tags(Input::get('email'));
			$telefono = strip_tags(Input::get('phone'));
			$pais = strip_tags(Input::get('country'));
			$provincia = strip_tags(Input::get('province'));
			$ciudad = strip_tags(Input::get('city'));
			$cp = strip_tags(Input::get('cp'));
			$direccion = strip_tags(Input::get('address'));
			$url = strip_tags(Input::get('url'));
			$pago = strip_tags(Input::get('pago'));

			$data = array(
				'nombre' => $nombre,
				'apellidos' => $apellidos,
				'email' => $email,
				'telefono' => $telefono,
				'pais' => $pais,
				'provincia' => $provincia,
				'ciudad' => $ciudad,
				'cp' => $cp,
				'direccion' => $direccion,
				'url' => $url
			);

			//Creamos un array con las reglas
			$rules = array(
				'nombre' => 'required',
				'apellidos' => 'required',
				'email' => 'required | email | unique:users',
				'telefono' => 'required',
				'pais' => 'required',
				'provincia' => 'required',
				'ciudad' => 'required',
				'cp' => 'required',
				'direccion' => 'required',
				'url' => 'required',
			);

			//Perdonalizamos mensajes de error
			$messages = array(
				'required' => 'El campo :attribute es obligatorio.',
				'email' => 'El campo :attribute ha ser un email válido.'
			);

			//Validamos
			$validation = Validator::make($data, $rules, $messages);

			//Comprobamos si los datos son erróneos
			if($validation->fails())
			{
				$errores = $validation->errors()->toArray();
				$mensaje = '';
				foreach($errores as $error)
				{
					foreach($error as $e)
					{
						$mensaje .= $error."<br />";
					}
				}

				return Redirect::back()->with('msg', $mensaje);
			}
			if($pago == 'paypal')
			{

			}
		}
		else {
			return Redirect::to('/carrito');
		}
	}

	//Vista finalizar pago
	public function getFinish()
	{
		$carrito = Session::get('carrito');
		unset($carrito['carrito']);
		Session::set('carrito', $carrito);
		return View::make('finish');
	}

}
