<?php
return array(
	// set your paypal credential
	'client_id' => 'AaLM0-Ue9gCzjVfa5bEKi4w67dKZjRA4CnlhWHe92fNAkzwZ5RtzvQPrpVIufAUHIPgp-u275Id9AkzP',
	'secret' => 'EPUhrb6CN6c756dWi3ACtuQG5DQiGAayCT1egJsjT02aDIRLM0SGrsGJSIWwB6KYv7OBUKOj8JqhWVFP',

	/**
	 * SDK configuration
	 */
	'settings' => array(
		/**
		 * Available option 'sandbox' or 'live'
		 */
		'mode' => 'sandbox',

		/**
		 * Specify the max request time in seconds
		 */
		'http.ConnectionTimeOut' => 30,

		/**
		 * Whether want to log to a file
		 */
		'log.LogEnabled' => true,

		/**
		 * Specify the file that want to write on
		 */
		'log.FileName' => storage_path() . '/logs/paypal.log',

		/**
		 * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
		 *
		 * Logging is most verbose in the 'FINE' level and decreases as you
		 * proceed towards ERROR
		 */
		'log.LogLevel' => 'FINE'
	),
);
