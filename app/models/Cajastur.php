<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 13/03/2015
 * Time: 15:15
 */

class Cajastur {

    private $merchant_id;
    private $acquirer_bin;
    private $terminal_id;
    private $clave_encriptacion;
    private $tipo_moneda;
    private $exponente;
    private $url_ok;
    private $url_error;
    private $pago_soportado;
    private $cifrado;
    private $idioma;
    private $descripcion;
    private $num_operacion;
    private $importe;
    private $referencia;

    public function Cajastur($num_operacion, $descripcion, $importe) {
        $this->merchant_id = "082108630";
        $this->acquirer_bin = "0000554002";
        $this->terminal_id = "00000003";
        $this->clave_encriptacion = "87401456";
        $this->tipo_moneda = "978";
        $this->exponente = "2";
        $this->url_ok = URL::to('finish');
        $this->url_error = URL::to('finish');
        $this->pago_soportado = "SSL";
        $this->cifrado = "SHA1";
        $this->idioma = "1";
        $this->descripcion = $descripcion;
        $this->num_operacion = $num_operacion;
        $this->importe = $importe;
        $this->referencia = "";
    }

    public function getFirma()
    {
        return sha1(
            $this->clave_encriptacion .
            $this->merchant_id .
            $this->acquirer_bin .
            $this->terminal_id .
            $this->num_operacion .
            $this->importe .
            $this->tipo_moneda .
            $this->exponente .
            $this->referencia .
            $this->cifrado .
            $this->url_ok .
            $this->url_error
        );
    }

    /**
     * @return string
     */
    public function getAcquirerBin()
    {
        return $this->acquirer_bin;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return string
     */
    public function getExponente()
    {
        return $this->exponente;
    }

    /**
     * @return string
     */
    public function getIdioma()
    {
        return $this->idioma;
    }

    /**
     * @return mixed
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * @return string
     */
    public function getMerchantId()
    {
        return $this->merchant_id;
    }

    /**
     * @return mixed
     */
    public function getNumOperacion()
    {
        return $this->num_operacion;
    }

    /**
     * @return string
     */
    public function getPagoSoportado()
    {
        return $this->pago_soportado;
    }

    /**
     * @return string
     */
    public function getTerminalId()
    {
        return $this->terminal_id;
    }

    /**
     * @return string
     */
    public function getTipoMoneda()
    {
        return $this->tipo_moneda;
    }

    /**
     * @return string
     */
    public function getUrlError()
    {
        return $this->url_error;
    }

    /**
     * @return string
     */
    public function getUrlOk()
    {
        return $this->url_ok;
    }

    /**
     * @return string
     */
    public function getCifrado()
    {
        return $this->cifrado;
    }

    /**
     * @return string
     */
    public function getReferencia()
    {
        return $this->referencia;
    }


} 