<?php

class Product extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	static public function total()
	{
		$total = 0;
		if(Session::has('carrito'))
		{
			foreach(Session::get('carrito') as $producto)
			{
				$total += Product::where('nombre_corto', '=', $producto)->first()->precio;
			}
		}
		return round(floatval($total), 2);
	}

}
