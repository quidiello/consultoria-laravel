<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 13/03/2015
 * Time: 19:11
 */

class Paypal {

    private $cmd;
    private $item_name;
    private $item_number;
    private $amount;
    private $currency_code;
    private $return;
    private $notify_url;

    public function Paypal($amount)
    {
        $this->cmd = "_cart";
        $this->item_name = "Pedido informes";
        $this->item_number = "INFORMES";
        $this->currency_code = "EUR";
        $this->return = URL::to('finish');
        $this->notify_url = URL::to('finish');
        $this->business = "example@example.com";
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getCmd()
    {
        return $this->cmd;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currency_code;
    }

    /**
     * @return mixed
     */
    public function getItemName()
    {
        return $this->item_name;
    }

    /**
     * @return mixed
     */
    public function getItemNumber()
    {
        return $this->item_number;
    }

    /**
     * @return mixed
     */
    public function getNotifyUrl()
    {
        return $this->notify_url;
    }

    /**
     * @return mixed
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * @return string
     */
    public function getBusiness()
    {
        return $this->business;
    }



} 