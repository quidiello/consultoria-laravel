<?php
/**
 * Created by PhpStorm.
 * User: Herminio
 * Date: 13/03/2015
 * Time: 16:04
 */

class Order extends Eloquent{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    static public function lastOrder()
    {
        return Order::max('id');
    }

} 