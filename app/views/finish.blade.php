<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="author" content="Eduardo Parrado, Herminio García y Xavier Quidiello">
        <title>Consultoría 3.0 | Procesando pago</title>

        <link rel="icon" type="image/ico" href="{{ asset('img/icon/favicon.ico') }}"/>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom-theme/jquery-ui-1.10.0.custom.css') }}">

        <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>

        <script src="{{ asset('js/finish.js') }}"></script>
    </head>
    <body>
    <div class="navbar-wrapper">
        <div class="container">
            <div class="jumbotron">
                <h1>Procesando su pedido</h1>
                <h2>En breves será redirigido al historial de pedidos</h2>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span class="sr-only">45% Complete</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>