@extends('layouts.base')

@section('title')
    | Principal
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
@stop

@section('body')

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="{{asset('images/seo-background.jpg')}}" alt="Foto de ponte traje">

                <div class="carousel-caption">
                    <h1>Informe SEO</h1>

                    <p>Mejora tu visibilidad en la web.</p>

                    <p><a class="btn btn-primary btn-lg" href="{{ url('/productos/seo') }}" role="button">Ver ahora</a></p>
                </div>
            </div>
            <div class="item">
                <img src="{{asset('images/seguridad-background.jpg')}}" alt="Foto modelo con abrigo rojo">

                <div class="carousel-caption">
                    <h1>Informe Seguridad</h1>

                    <p>Protégete, protege a tus clientes.</p>

                    <p><a class="btn btn-primary btn-lg" href="{{ url('/productos/seguridad') }}" role="button">Ver ahora</a></p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <div id="alertAddCart" class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Producto añadido</strong> Se han añadido los productos seleccionados, vaya al <a href="{{ url('/carrito') }}">carrito</a>
        para verlos.
    </div>

    <div class="container content">
        <div class="row">
            @if($productos->count())
                @foreach($productos as $producto)
                    <div class="col-md-4">
                        <a href="{{ url('/productos/'.$producto->nombre_corto) }}">
                            <img class="img-circle" src="{{ asset('images/'.$producto->imagen) }}"></a>

                        <div class="caption">
                            <h3>{{$producto->nombre}}</h3>
                            <h4>{{$producto->precio}} €</h4>

                            <div class="btn-group">
                                <a href="#" onclick="addItem('{{$producto->nombre_corto}}');" type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-shopping-cart"></span> Añadir al carrito</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

    </div>

@stop
