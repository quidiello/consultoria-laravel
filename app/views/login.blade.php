@extends('layouts.base')

@section('title')
    | Producto
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">

    <script src="{{ asset('js/login.js') }}"></script>
@stop

@section('body')

    <div class="container">
        <div id="login" class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <h1 class="text-center login-title">Iniciar sesión en Consultoría 3.0</h1>
                <div class="account-wall">
                    <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                         alt="">
                    <!-- Mostrar mensaje en caso de que exista -->
                    @if(Session::get('msg'))
                        <p class="resaltar">{{ Session::get('msg') }}</p>
                    @endif
                    {{ Form::open(array('class' => 'form-signin', 'url' => '/login', 'method' => 'POST')) }}
                        <input type="text" name="email" class="form-control" placeholder="Email" required autofocus>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Iniciar sesión">
                    {{ Form::close() }}
                </div>
                <a href="{{ url('/registro') }}" class="text-center new-account">Registrarse </a>
            </div>
        </div>
    </div>

@stop
