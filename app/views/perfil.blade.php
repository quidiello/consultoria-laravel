@extends('layouts.base')

@section('title')
    | Perfil
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">

    <script src="{{ asset('js/profile.js') }}"></script>
@stop

@section('body')

    <div class="container content">
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li class="active">Mi perfil</li>
        </ol>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#orders" role="tab" data-toggle="tab">Pedidos</a></li>
            <li role="presentation"><a href="#profile" role="tab" data-toggle="tab">Mis datos</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="orders">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fecha</th>
                        <th>Estado</th>
                        <th>Precio</th>
                        <th>Descripción</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    <tr>
                        <td>4537835</td>
                        <td>15/03/2015</td>
                        <td>En proceso</td>
                        <td>200 €</td>
                        <td>1 x Informe Accesibilidad</td>
                    </tr>
                    <tr>
                        <td>4537835</td>
                        <td>12/03/2015</td>
                        <td>Entregado</td>
                        <td>300 €</td>
                        <td>1 x Informe Seguridad</td>
                    </tr>
                    <tr>
                        <td>5343443</td>
                        <td>12/08/2014</td>
                        <td>Entregado</td>
                        <td>150€</td>
                        <td>1 x Informe Usabilidad</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
                {{ Form::open(array('class' => 'form-horizontal', 'id' => 'datos_usuario', 'url' => '/perfil', 'method' => 'POST')) }}
                    <fieldset>
                        <div class="form-group">
                            <label for="name" class="col-md-2">Nombre: </label>
                            <input name="name" type="text" class="col-md-10" value="{{$usuario->nombre}}">
                        </div>
                        <div class="form-group">
                            <label for="surnames" class="col-md-2">Apellidos: </label>
                            <input name="surnames" type="text" class="col-md-10" value="{{$usuario->apellidos}}">
                        </div>
                        <div class="form-group">
                            <label for="mail" class="col-md-2">Correo electrónico: </label>
                            <input name="mail" type="text" class="col-md-10" value="{{$usuario->email}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-2">Teléfono: </label>
                            <input name="phone" type="text" class="col-md-10" value="{{$usuario->telefono}}">
                        </div>
                        <div class="form-group">
                            <label for="country" class="col-md-2">País: </label>
                            <input name="country" type="text" class="col-md-10" value="{{$usuario->pais}}">
                        </div>
                        <div class="form-group">
                            <label for="province" class="col-md-2">Provincia: </label>
                            <input name="province" type="text" class="col-md-10" value="{{$usuario->provincia}}">
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-md-2">Ciudad: </label>
                            <input name="city" type="text" class="col-md-10" value="{{$usuario->ciudad}}">
                        </div>
                        <div class="form-group">
                            <label for="cp" class="col-md-2">Código postal: </label>
                            <input name="cp" type="text" class="col-md-10" value="{{$usuario->cp}}">
                        </div>
                        <div class="form-group">
                            <label for="street" class="col-md-2">Dirección: </label>
                            <input name="street" type="text" class="col-md-10" value="{{$usuario->direccion}}">
                        </div>
                        <div class="form-group pull-right">
                            <a href="{{ url('/perfil') }}" class="btn btn-default">Cancelar</a>
                            <a href="#" class="btn btn-success" onclick="actualizarUsuario();" >Actualizar mis datos</a>
                        </div>
                    </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop
