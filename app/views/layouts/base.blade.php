<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="author" content="Eduardo Parrado, Herminio García y Xavier Quidiello">
    <title>Consultoría 3.0 @yield('title')</title>

    <link rel="icon" type="image/ico" href="{{ asset('img/icon/favicon.ico') }}"/>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom-theme/jquery-ui-1.10.0.custom.css') }}">

    <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('js/navbar.js') }}"></script>
    <script src="{{ asset('js/product.js') }}"></script>

    @yield('head')

</head>
<body>

<div class="navbar-wrapper">
    <div class="container">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">Consultoría 3.0</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/productos') }}">Productos</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/carrito') }}"><span class="glyphicon glyphicon-shopping-cart"></span>
                                Carrito <span id="cartCount" class="badge">{{count(Session::get('carrito'))}}</span></a></li>
                        <li><a href="{{ url('/perfil') }}"><span class="glyphicon glyphicon-user"></span> Mi perfil</a>
                        </li>
                        @if(Auth::check())
                        <li><a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-remove-sign"></span> Cerrar sesión</a>
                        </li>
                        @endif
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>
    </div>
</div>

@yield('body')

</body>
</html>
