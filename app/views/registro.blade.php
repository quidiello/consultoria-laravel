@extends('layouts.base')

@section('title')
    | Producto
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
@stop

@section('body')

    <div class="container content">
        @if(Session::has('msg'))
            <p class="resaltar">{{Session::get('msg')}}</p>
        @endif
        {{ Form::open(array('class' => 'form-horizontal', 'url' => '/registro', 'method' => 'POST')) }}
            <fieldset>
                <div class="form-group">
                    <label for="name" class="col-md-2">Nombre: </label>
                    <input name="name" type="text" class="col-md-10" required>
                </div>
                <div class="form-group">
                    <label for="surnames" class="col-md-2">Apellidos: </label>
                    <input name="surnames" type="text" class="col-md-10" required>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-2">Correo electrónico: </label>
                    <input name="email" type="text" class="col-md-10" required>
                </div>
                <div class="form-group">
                    <label for="passowrd" class="col-md-2">Contraseña: </label>
                    <input name="password" type="password" class="col-md-10" required>
                </div>
                <div class="form-group">
                    <label for="password2" class="col-md-2">Confirmar contraseña: </label>
                    <input name="password2" type="password" class="col-md-10" required>
                </div>

                <div class="form-group">
                    <input class="btn btn-success pull-right" type="submit" value="Registrar">
                </div>
            </fieldset>
        {{ Form::close() }}
    </div>

@stop
