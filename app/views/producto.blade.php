@extends('layouts.base')

@section('title')
    | Producto
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/product1.css') }}">

    <script src="{{ asset('js/product1.js') }}"></script>
@stop

@section('body')

    <div class="container content">
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ url('/productos') }}">Productos</a></li>
            <li class="active">{{$producto->nombre_corto}}</li>
        </ol>
        <div class="row panel panel-default">
            <div class="panel-heading">
                Ficha producto
            </div>
            <div class="panel-body">

            </div>
            <div class="col-md-4 leftColumnProduct">
                <div class="parent-container">
                    <a href="{{asset('images/'.$producto->imagen)}}"><img src="{{asset('images/'.$producto->imagen)}}" alt=""></a>
                </div>
                <h4>{{$producto->precio}} €</h4>

                <div class="btn-group">
                    <a href="#" data-toggle="modal" data-target="#configureModal" type="button" class="btn btn-success">
                        <span class="glyphicon glyphicon-shopping-cart"></span> Añadir al carrito</a>
                </div>
            </div>
            <div class="col-md-8">
                <h2>{{$producto->nombre}}</h2>

                <p>{{$producto->descripcion}}</p>
            </div>
        </div>

    </div>

@stop
