@extends('layouts.base')

@section('title')
    | Productos
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ asset('css/products.css') }}">

    <script src="{{ asset('js/product.js') }}"></script>
@stop

@section('body')

    <div class="container content">
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li class="active">Productos</li>
        </ol>
        <div id="alertAddCart" class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Producto añadido</strong> Se han añadido los productos seleccionados, vaya al <a href="{{ url('/carrito') }}">carrito</a>
            para verlos.
        </div>
        <div class="row">

            @if($productos->count())
                @foreach($productos as $producto)
                    <div class="col-md-4">
                        <a href="{{ url('/productos/'.$producto->nombre_corto) }}">
                            <img class="img-circle" src="{{ asset('images/'.$producto->imagen) }}"></a>

                        <div class="caption">
                            <h3>{{$producto->nombre}}</h3>
                            <h4>{{$producto->precio}} €</h4>

                            <div class="btn-group">
                                <a href="#" onclick="addItem('{{$producto->nombre_corto}}');" type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-shopping-cart"></span> Añadir al carrito</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>

@stop
