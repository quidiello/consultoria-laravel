@extends('layouts.base')

@section('title')
    | Formulario
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">

    <script src="{{ asset('js/form.js') }}"></script>
@stop

@section('body')

    <div class="container content">
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ url('/carrito') }}">Carrito</a></li>
            <li class="active">Formulario de compra</li>
        </ol>
            <form class="form-horizontal">
            <fieldset>
                <div class="panel panel-default">
                    <div class="panel-heading">Datos del envío</div>
                    <div id="chooseAddress" class="panel-body">
                        <div class="form-group btn-group col-lg-12">
                            <button onclick="fillAddress();" type="button" class="btn btn-default">Usar mis datos
                                habitual
                            </button>
                            <button onclick="clearAddress();" type="button" class="btn btn-default">Introducir nuevos datos
                            </button>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="name" class="col-md-2">Nombre: </label>
                            <input name="name" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="surnames" class="col-md-2">Apellidos: </label>
                            <input name="surnames" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-2">Correo electrónico: </label>
                            <input name="email" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-2">Teléfono: </label>
                            <input name="phone" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="country" class="col-md-2">País: </label>
                            <input name="country" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="province" class="col-md-2">Provincia: </label>
                            <input name="province" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-md-2">Ciudad: </label>
                            <input name="city" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label name="cp" class="col-md-2">Código postal: </label>
                            <input name="cp" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-md-2">Dirección: </label>
                            <input name="address" type="text" class="col-md-8">
                        </div>
                        <div class="form-group">
                            <label for="url" class="col-md-2">URL a analizar: </label>
                            <input name="url" type="text" class="col-md-8">
                        </div>
                    </div>
                </div>
                </fieldset>
                </form>
                <div class="panel panel-default">
                    <div class="panel-heading">Método de Pago</div>
                     <div id="choosePaymentMethod" class="panel-body">
                                                                 <div class="form-group btn-group col-lg-12">
                                                                     <form action="http://tpv.ceca.es:8000/cgi-bin/tpv" method="post" enctype="application/x-www-form-urlencoded">
                                                                         <input name="MerchantID" type="hidden" value="{{ $cajastur->getMerchantId() }}">
                                                                         <input name="AcquirerBIN" type="hidden" value="{{ $cajastur->getAcquirerBin() }}">
                                                                         <input name="TerminalID" type="hidden" value="{{ $cajastur->getTerminalId() }}">
                                                                         <input name="URL_OK" type="hidden" value="{{ $cajastur->getUrlOk() }}">
                                                                         <input name="URL_NOK" type="hidden" value="{{ $cajastur->getUrlError() }}">
                                                                         <input name="Firma" type="hidden" value="{{ $cajastur->getFirma() }}">
                                                                         <input name="Num_operacion" type="hidden" value="{{ $cajastur->getNumOperacion() }}">
                                                                         <input name="Importe" type="hidden" value="{{ $cajastur->getImporte() }}">
                                                                         <input name="TipoMoneda" type="hidden" value="{{ $cajastur->getTipoMoneda() }}">
                                                                         <input name="Exponente" type="hidden" value="{{ $cajastur->getExponente() }}">
                                                                         <input name="Pago_soportado" type="hidden" value="{{ $cajastur->getPagoSoportado() }}">
                                                                         <input name="Idioma" type="hidden" value="1">
                                                                         <input name="Cifrado" type="hidden" value="{{ $cajastur->getCifrado() }}">
                                                                         <input type="submit" id="bTarjeta" type="button" class="btn btn-success" value="Tarjeta">
                                                                     </form>
                                                                     <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                                                                         <input name="cmd" type="hidden" value="{{ $paypal->getCmd() }}">
                                                                         <input name="upload" type="hidden" value="1">
                                                                         <input name="business" type="hidden" value="{{ $paypal->getBusiness() }}">
                                                                         <input name="item_name_1" type="hidden" value="{{ $paypal->getItemName() }}">
                                                                         <input name="item_number_1" type="hidden" value="{{ $paypal->getItemNumber() }}">
                                                                         <input name="amount_1" type="hidden" value="{{ $paypal->getAmount() }}">
                                                                         <input name="currency_code" type="hidden" value="{{ $paypal->getCurrencyCode() }}">
                                                                         <input name="return" type="hidden" value="{{ $paypal->getReturn() }}">
                                                                         <input name="notify_url" type="hidden" value="{{ $paypal->getNotifyUrl() }}">
                                                                         <input type="submit" id="bPaypal" type="button" class="btn btn-primary" value="Paypal">
                                                                     </form>
                                                                 </div>
                                                                 </div>
                                                                 </div>
                <div class="form-group pull-right">
                    <a href="{{ url('/carrito') }}" class="btn btn-default">Cancelar</a>
                </div>


    </div>

@stop
