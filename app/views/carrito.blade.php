@extends('layouts.base')

@section('title')
    | Carrito
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">

    <script src="{{ asset('js/cart.js') }}"></script>
@stop

@section('body')

    <div class="container content">
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}">Inicio</a></li>
            <li class="active">Carrito</li>
        </ol>
        <div class="row">
            <div class="panel-group col-md-8" id="accordion" role="tablist" aria-multiselectable="true">

                @if(count(Session::get('carrito')))
                    <?php $contador = 0; ?>
                    @foreach(Session::get('carrito') as $producto)
                        <?php $contador++; ?>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a href="#" onclick="removeItem(this);" class="pull-right" data-toggle="tooltip" data-producto="{{$producto}}"
                                       data-placement="top" title="Pinchar para eliminar"><span
                                                class="glyphicon glyphicon-trash"></span></a>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse{{$contador}}"
                                       aria-expanded="false" aria-controls="collapseThree">
                                        <span>{{Product::where('nombre_corto', '=', $producto)->first()->nombre}}</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{$contador}}" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <div class="container">
                                        <div class="row">
                                            <div class="leftColumnPhoto col-md-2">
                                                <img class="itemPhoto img-circle" src="{{asset('images/'.Product::where('nombre_corto', '=', $producto)->first()->imagen)}}" alt="">
                                            </div>
                                            <div class="col-md-4">
                                                <p>{{Product::where('nombre_corto', '=', $producto)->first()->descripcion}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif
            </div>

            <div id="budget" class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading table-responsive">Precio total</div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Producto</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody id="tablebody">
                        @if(Session::get('carrito'))
                            <?php $contador = 0; ?>
                            @foreach(Session::get('carrito') as $producto)
                                <?php $contador++; ?>
                                <tr id="{{$producto}}" data-precio="{{Product::where('nombre_corto', '=', $producto)->first()->precio}}">
                                    <td>{{$contador}}.</td>
                                    <td>{{Product::where('nombre_corto', '=', $producto)->first()->nombre}}</td>
                                    <td>{{Product::where('nombre_corto', '=', $producto)->first()->precio}} €</td>
                                </tr>
                            @endforeach
                            <tr class="resaltar">
                                <td></td>
                                <td>TOTAL: </td>
                                <td id="total">{{number_format(Product::total(),2)}} €</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <a href="{{ url('/formulario_compra') }}" class="btn btn-success pull-right">Comprar</a>
            </div>
        </div>
    </div>

@stop
