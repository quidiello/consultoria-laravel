<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'nombre' => 'Xavier',
            'apellidos' => 'Quidiello Suárez',
            'email' => 'uo204254@uniovi.es',
            'password' => Hash::make('909042'),
            'telefono' => '622028663',
            'pais' => 'España',
            'provincia' => 'Asturias',
            'ciudad' => 'Gijón',
            'cp' => '33212',
            'direccion' => 'Calle Chile, nº 13, 5º D'
        ));

    }

}

