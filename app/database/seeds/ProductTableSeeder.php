<?php

class ProductTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('products')->delete();

        Product::create(array(
            'nombre' => 'Informe Accesibilidad',
            'nombre_corto' => 'accesibilidad',
            'descripcion' => 'Con este informe de accesibilidad usted podrá saber si está permitiendo que gente con alguna discapacidad pueda navegar adecuadamente por su página.',
            'precio' => 200,
            'imagen' => 'accesibilidad.jpeg'
        ));

        Product::create(array(
            'nombre' => 'Informe Usabilidad',
            'nombre_corto' => 'usabilidad',
            'descripcion' => 'Con este informe de usabilidad usted podrá saber si su página se puede usar de una manera fácil o si por el contrario sus usuarios la encuentran tediosa y se pierden.',
            'precio' => 150,
            'imagen' => 'usabilidad.jpeg'
        ));

        Product::create(array(
            'nombre' => 'Informe Seguridad',
            'nombre_corto' => 'seguridad',
            'descripcion' => '¿Quiere mantener a los hackers alejados de su página web? Con este informe descubrirá las vulnerabilidades de su sistema.',
            'precio' => 300,
            'imagen' => 'seguridad.jpeg'
        ));

        Product::create(array(
            'nombre' => 'Informe SEO',
            'nombre_corto' => 'SEO',
            'descripcion' => '¿Sus clientes no le encuentran en Google? ¡Eso tiene remedio! Con este informe descubrirá cuáles son los defectos de su página y aprenderá a poscionarse mejor en Google y otros buscadores.',
            'precio' => 350,
            'imagen' => 'seo.jpeg'
        ));


    }

}

