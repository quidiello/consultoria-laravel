function fillAddress() {

    $.ajax({
        type: 'POST',
        url: 'usuario',
        success: function(request) {
            $("input[name=name]").val(request.nombre);
            $("input[name=surnames]").val(request.apellidos);
            $("input[name=email]").val(request.email);
            $("input[name=phone]").val(request.telefono);
            $("input[name=country]").val(request.pais);
            $("input[name=province]").val(request.provincia);
            $("input[name=city]").val(request.ciudad);
            $("input[name=cp]").val(request.cp);
            $("input[name=address]").val(request.direccion);
        }
    });

}

function clearAddress() {
    $("input[name=name]").val("");
    $("input[name=surnames]").val("");
    $("input[name=email]").val("");
    $("input[name=phone]").val("");
    $("input[name=country]").val("");
    $("input[name=province]").val("");
    $("input[name=city]").val("");
    $("input[name=cp]").val("");
    $("input[name=address]").val("");
    $("input[name=url]").val("");
}

function selectTarjeta() {
    $("#bPaypal").attr('class', 'btn btn-default');
    $("#bTarjeta").attr('class', 'btn btn-info');
}

function selectPaypal() {
    $("#bTarjeta").attr('class', 'btn btn-default');
    $("#bPaypal").attr('class', 'btn btn-info');
}