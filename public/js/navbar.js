$(function() {
            $("#price-slider").slider({
                range: true,
                min: 0,
                max: 1000,
                values: [0, 1000],
                slide: function( event, ui ) {
                    $( "#amount" ).val(ui.values[0] + "€" + " - " + ui.values[1] + "€");
                }
            });
            $( "#amount" ).val($( "#price-slider" ).slider( "values", 0 ) + "€" + " - " + $( "#price-slider" ).slider( "values", 1 ) + "€");
        });