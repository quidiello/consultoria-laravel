$(document).ready(function() {
    function increaseProgressBar(percentage) {
        if(percentage < 100) {
            percentage += 10;
            setTimeout(function() {
                console.log(percentage);
                $(".progress-bar").attr('aria-valuenow', percentage);
                $(".progress-bar").css('width', percentage + '%');
                increaseProgressBar(percentage);
            }, 1000);
        } else {
            $("h1").text("Pedido guardado");
            $("h2").show();
            setTimeout(function() {
                window.location.href = "perfil";
            }, 3000);
        }
    } increaseProgressBar(0);
});