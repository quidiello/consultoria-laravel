function removeItem(childElement) {
    childElement.parentNode.parentNode.parentNode.remove();

    var nombre = $(childElement).data('producto');
    $.ajax({
        type: 'POST',
        url: 'remove/' + nombre,
        beforeSend: function () {

        },
        error: function () {

        },
        success: function (request) {
            if(request == 0)
            {
                var currentNumber = parseInt($("#cartCount").text());
                $("#cartCount").text(--currentNumber);

                var precio = parseFloat($('#'+nombre).data('precio'));
                var total = parseFloat($('#total').text().replace(",",""));
                total = total - precio;
                $('#total').text(total.toFixed(2) + ' €');
                $('#'+nombre).remove();
            }
        }
    });
}

