function showAddCartAlert() {
    $("#alertAddCart").show();
}

function addItem(nombre) {

    $.ajax({
        type: 'POST',
        url: 'add/' + nombre,
        beforeSend: function () {

        },
        error: function () {

        },
        success: function (request) {
            if(request == 0)
            {
                var currentNumber = parseInt($("#cartCount").text());
                $("#cartCount").text(++currentNumber);
                showAddCartAlert();
            }
        }
    });

}